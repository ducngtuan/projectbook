class AddAboutAndSearchToUser < ActiveRecord::Migration
  def change
    add_column :users, :about, :text
    add_column :users, :search, :text
  end
end
