class AddProjectIdToFaculty < ActiveRecord::Migration
  def change
    add_column :faculties, :project_id, :integer
  end
end
