class RenameProjectsCompanies < ActiveRecord::Migration
  def up
    rename_table :projects_companies, :companies_projects
  end
  

  def down
    rename_table :companies_projects, :projects_companies
  end
end
