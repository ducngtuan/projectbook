class RemoveProjectIdFromFaculty < ActiveRecord::Migration
  def up
    remove_column :faculties, :project_id
  end

  def down
    add_column :faculties, :project_id, :integer
  end
end
