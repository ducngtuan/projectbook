class RenameFacultyColumn < ActiveRecord::Migration
  def change
    rename_column :faculties, :facultyname, :name
  end
end
