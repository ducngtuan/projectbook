class AddFacultyIdToProject < ActiveRecord::Migration
  def change
    add_column :projects, :faculty_id, :integer
  end
end
