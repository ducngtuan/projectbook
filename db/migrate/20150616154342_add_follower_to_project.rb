class AddFollowerToProject < ActiveRecord::Migration
  def change
    create_table :projects_followers, id: false do |t|
      t.references :project
      t.references :user
    end
    add_index :projects_followers, [:project_id, :user_id]
    add_index :projects_followers, :user_id
  end
end
