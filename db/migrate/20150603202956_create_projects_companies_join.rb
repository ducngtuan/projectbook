class CreateProjectsCompaniesJoin < ActiveRecord::Migration
  def up
    create_table 'projects_companies', :id => false do |t|
      t.column 'project_id', :integer
      t.column 'company_id', :integer
    end
  end

  def down
    drop_table 'projects_companies'
  end
end
