class RenameProjectColumn < ActiveRecord::Migration
  def change
    rename_column :projects, :beschreibung, :description
  end
end
