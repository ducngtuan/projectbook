class AddDescriptiomColumnsToProjects < ActiveRecord::Migration
  def change
    add_column :projects, :short_description, :text
    add_column :projects, :long_description, :text
  end
end
