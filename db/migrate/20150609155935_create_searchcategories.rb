class CreateSearchcategories < ActiveRecord::Migration
  def change
    create_table :searchcategories do |t|
      t.string :name

      t.timestamps
    end
  end
end
