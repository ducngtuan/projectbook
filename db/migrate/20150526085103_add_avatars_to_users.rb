class AddAvatarsToUsers < ActiveRecord::Migration
  def self.up
    change_table :users do |t|
      t.attachment :avatar
    end
  end
  
  def self.down
    drop_attachment_file :users, :avatar
  end
end
