class AddCallsToProjects < ActiveRecord::Migration
  def change
    add_column :projects, :calls, :integer
  end
end
