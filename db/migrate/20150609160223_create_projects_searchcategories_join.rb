class CreateProjectsSearchcategoriesJoin < ActiveRecord::Migration
  def up
    create_table 'projects_searchcategories', :id => false do |t|
      t.column 'searchcategory_id', :integer
      t.column 'project_id', :integer
    end
    
  end

  def down
    drop_table 'projects_searchcategories'
  end
end
