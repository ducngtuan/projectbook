class HomepageController < ApplicationController
  
  def home  
    @projectcount = 5
    @topProjects = Project.all.sort_by { |project| [project.followers.count]}.reverse.take(@projectcount)
    @uptodateProjects = Project.all.sort_by { |project| [project.created_at]}.reverse.take(@projectcount)
  end
end
