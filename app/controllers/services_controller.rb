class ServicesController < ApplicationController
  def contact
  end

  def about_us
  end

  def terms_of_use
  end

  def privacy
  end
  
  def faqs
  end
end
