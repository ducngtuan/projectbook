class ApplicationController < ActionController::Base  
   add_crumb "Startseite", :root_path  
      
  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:username, :email, :password, :password_confirmation, :remember_me) }
    devise_parameter_sanitizer.for(:sign_in) { |u| u.permit(:login, :username, :email, :password, :remember_me) }
    devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:firstname, :surname, :username, :email, :faculty_id, :about, :search, :password, :password_confirmation, :current_password, :avatar) }
  end
   
  protect_from_forgery
end
