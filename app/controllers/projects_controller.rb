# encoding: utf-8
class ProjectsController < ApplicationController    
  add_crumb "Projekte", :projects_path
  # GET /projects
  # GET /projects.json  
  def index  
    if params[:faculty] == ""
      redirect_to('/')
      return
    end
    
    @headingText = headingText
    searchtag = (params[:search])
    
    if params[:faculty] == "0" || params[:faculty] == nil
      if params[:search]		
        @projects = Project.find(:all, :conditions => 
			['name LIKE ? OR short_description LIKE ? OR long_description LIKE ?', "%#{searchtag}%", "%#{searchtag}%", "%#{searchtag}%"])  
      else
        @projects = Project.find(:all)
      end
    else
      if params[:search]
        @projects = Project.find_all_by_faculty_id(params[:faculty], 
          :conditions => ['name LIKE ? OR short_description LIKE ? OR long_description LIKE ?', "%#{searchtag}%", "%#{searchtag}%" , "%#{searchtag}%"])  
      else
        @projects = Project.find_all_by_faculty_id(params[:faculty])
      end
      add_crumb 'Fakultät '+ params[:faculty], '../../projects/faculty'+params[:faculty]
    end
      
    case params[:sort]      
      when "Erstellungsdatum"
        @projects.sort_by! {|u| u.created_at}
        @projects.reverse!  
      when "Aufrufe"
        @projects.sort_by! {|u| u.calls}   
        @projects.reverse!
      when "Follower"
        @projects.sort_by! {|u| u.followers.count}   
        @projects.reverse!    
      else 
        @projects.sort_by! {|u| u.name}
    end
          
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @projects }
    end
  end

  def headingText
    if params[:search] != nil
      text = "Alle Suchergebnisse für '#{params[:search]}'"
    elsif params[:faculty] == "0" || params[:faculty] == nil
      text = 'Alle Projekte'
    else      
      text = "Projekte der Fakultät #{params[:faculty]} - #{Faculty.find_by_id(params[:faculty]).name}"
    end
    return text
  end

  def facultyBreadcrumb
    add_crumb 'Fakultät '+ @project.faculty_id.to_s, '../../projects/faculty/' + @project.faculty_id.to_s
  end
  
  # GET /projects/1
  # GET /projects/1.json
  def show
    @project = Project.find(params[:id])
    @project.calls = @project.calls + 1
    @project.save    
    facultyBreadcrumb()
    add_crumb @project.name, :project_path 
    
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @project }
    end
  end

  # GET /projects/new
  # GET /projects/new.json
  def new
    @project = Project.new  
 
    add_crumb 'Neues Projekt', ''
    
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @project }
    end
  end

  # GET /projects/1/edit
  def edit
    @project = Project.find(params[:id])    
    facultyBreadcrumb
    add_crumb @project.name, '../' + params[:id]    
    add_crumb 'Bearbeiten',  ''
   
  end

  # POST /projects
  # POST /projects.json
  def create
    @project = Project.new(params[:project])
    @project.calls = 0
    @project.users << current_user    
    @project.save
    
    @users = @project.users
    @companies = @project.companies
    
    @teammembers = params[:teammember]
    @deleteteammembers = params[:deleteteammember]
    @sponsors = params[:sponsor]
    @deletesponsors = params[:deletesponsor]
    
    unless @teammembers.nil?
    @teammembers.each do |t|
    unless @users.exists?(t)
       @users << User.find(t)
      end
    end
    end
    
    unless @deleteteammembers.nil?
    @deleteteammembers.each do |t|
      if @users.exists?(t)
       @users.delete(User.find(t))
      end
    end
    end
    
    unless @sponsors.nil?
    @sponsors.each do |t|
    unless @companies.exists?(t)
       @companies << Company.find(t)
      end
    end
    end
    
    unless @deletesponsors.nil?
    @deletesponsors.each do |t|
    if @companies.exists?(t)
       @companies.delete(Company.find(t))
      end
    end
    end
    
    
    respond_to do |format|
      if @project.save
        format.html { redirect_to @project, notice: 'Projekt erfolgreich erstellt.' }
        format.json { render json: @project, status: :created, location: @project }
      else
        format.html { render action: "new" }
        format.json { render json: @project.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /projects/1
  # PUT /projects/1.json
  def update
    @project = Project.find(params[:id])
    @users = @project.users
    @companies = @project.companies
    @milestones = @project.milestones
    @events = @project.events

    @teammembers = params[:teammember]
    @deleteteammembers = params[:deleteteammember]
    @sponsors = params[:sponsor]
    @deletesponsors = params[:deletesponsor]
    @deletemilestones = params[:deletemilestone]
    @deleteevents = params[:deleteevent]
       
    unless @deletetemilestones.nil?        
      @deletemilestones.each do |t|        
         @milestones.delete(Milestone.find(t))   
      end
    end
    
    unless @deletetemilestones.nil?
    @deleteevents.each do |t|        
       @project.events.delete(Event.find(t))   
    end
    end
    
    unless @teammembers.nil?
    @teammembers.each do |t|
    unless @users.exists?(t)
       @users << User.find(t)
      end
    end
    end
    
    unless @deleteteammembers.nil?
    @deleteteammembers.each do |t|      
       @users.delete(User.find(t))      
    end
    end
    
    unless @sponsors.nil?
    @sponsors.each do |t|
    unless @companies.exists?(t)
       @companies << Company.find(t)
      end
    end
    end
    
    unless @deletesponsors.nil?
    @deletesponsors.each do |t|
    if @companies.exists?(t)
       @companies.delete(Company.find(t))
      end
    end
    end
         
    respond_to do |format|
      if @project.update_attributes(params[:project])
        format.html { redirect_to @project, notice: 'Projekt erfolgreich geändert.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @project.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /projects/1
  # DELETE /projects/1.json
  def destroy
    @project = Project.find(params[:id])
    @project.destroy

    respond_to do |format|
      format.html { redirect_to projects_url }
      format.json { head :no_content }
    end
  end
  
  # POST /projects/1/follow
  # POST /projects/1/follow.json  
  def follow
    @project = Project.find(params[:id])
    @project.followers << current_user

    respond_to do |format|
      format.html { redirect_to @project, notice: 'Du folgst nun diesem Projekt.' }
      format.json { head :no_content }
    end
  end

  # DELETE /projects/1/unfollow
  # DELETE /projects/1/unfollow.json
  def unfollow
    @project = Project.find(params[:id])
    @project.followers.delete(current_user)

    respond_to do |format|
      format.html { redirect_to @project, notice: 'Du folgst diesem Projekt nicht mehr.' }
      format.json { head :no_content }
    end
  end
end
