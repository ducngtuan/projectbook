class AttachmentsController < ApplicationController
  before_filter :set_attachment, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    project = Project.find(params[:project_id])
    @attachments = project.attachments
    respond_with(@attachments)
  end

  def show
    project = Project.find(params[:project_id])
	@attachment = project.attachments.find(params[:id])
	    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @attachment }
    end
  end

  def new
    project = Project.find(params[:project_id])
	@attachment = project.attachments.build
	    respond_to do |format|
      format.html # new.html.erb
    end
  end

  def edit
  project = Project.find(params[:project_id])
  @attachment = project.attachment.find(params[:id])
  end

  def create
  
    project = Project.find(params[:project_id])
    @attachment = project.attachments.create(params[:attachment])
	
	  respond_to do |format|
      if @attachment.save
  
        format.html { redirect_to([@attachment.project, @attachment], :notice => 'Attachment was successfully created.') }

      else
        format.html { render :action => "new" }

      end
    end


  end

  def update

    project = Project.find(params[:project_id])

    @attachment = project.attachments.find(params[:id])

    respond_to do |format|
      if @attachment.update_attributes(params[:attachment])

        format.html { redirect_to([@attachment.project, @attachment], :notice => 'Attachment was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @attachment.errors, :status => :unprocessable_entity }
      end
    end

  end

  def destroy
	project = Project.find(params[:project_id])
	@attachment = project.attachments.find(params[:id])
	@attachment.destroy

	    respond_to do |format|

      format.html { redirect_to(project_attachments_url) }
      format.xml  { head :ok }
	  end
  end

  private
    def set_attachment
      @attachment = Attachment.find(params[:id])
    end
end
