class UsersController < ApplicationController
#before_filter :authenticate_user!

  def show
    add_crumb "Mein Profil", :user_path
    
    @ProjectMember = current_user.projects
    @ProjectFollower = current_user.followed_projects
    
    if (current_user.firstname == nil || current_user.surname == nil) ||
        (current_user.firstname == "" || current_user.surname == "")
        @profilfrom = current_user.username
    else
        @profilfrom = current_user.firstname + " " + current_user.surname
    end
    #@users = User.all
    
    #@user = User.find(params[:id])

    respond_to do |format|
        format.html # show.html.erb
        format.xml { render :xml => @user }
    end
  end
  
  def index
    @users = User.all
    @users = User.search(params[:search])
  end  
end