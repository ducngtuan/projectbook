# encoding: utf-8
class ForumController < ApplicationController
  def dummy
     @project = Project.find(params[:id])   
     add_crumb "Projekte", '../../projects'
     add_crumb 'Fakultät '+ @project.faculty_id.to_s, '../../projects/faculty/' + @project.faculty_id.to_s
     add_crumb @project.name, '../../projects/' + params[:id] 
     add_crumb 'Forum',''
  end
end
