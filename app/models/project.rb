class Project < ActiveRecord::Base
  belongs_to :faculty
  has_and_belongs_to_many :users
  has_many :milestones
  has_many :events
  has_and_belongs_to_many :companies
  has_and_belongs_to_many :searchcategories
  has_and_belongs_to_many :followers,
      class_name: "User",
      association_foreign_key: "user_id",
      join_table: "projects_followers"
    has_many :attachments

  
  attr_accessor :avatar
  attr_accessible :short_description, :name, :faculty_id, :avatar, :long_description
  
  has_attached_file :avatar, :styles => { :medium => "300x300#", :thumb => "100x100#", :rect => "500x250#" }, :default_url => "/images/:style/projectdefault.png"
  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/
  validates_attachment_size :avatar, :less_than => 2.megabytes
  
  validates_presence_of :name, :short_description
  
  def self.search(search)
    if search
      find(:all, :conditions => ['name LIKE ?', "%#{search}%"])
    else
      find(:all)
    end
  end
end
