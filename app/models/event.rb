class Event < ActiveRecord::Base
  belongs_to :project
  
  attr_accessible :name
  attr_accessible :project_id
end
