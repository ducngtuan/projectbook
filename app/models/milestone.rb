class Milestone < ActiveRecord::Base
 belongs_to :project
  
  attr_accessible :date, :name, :description, :project_id
end
