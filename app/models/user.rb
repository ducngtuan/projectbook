class User < ActiveRecord::Base
  has_one :faculty
  has_and_belongs_to_many :projects
  has_and_belongs_to_many :followed_projects,
      class_name: "Project",
      foreign_key: "user_id",
      join_table: "projects_followers"
    has_many :attachments

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :authentication_keys => [:login]

  # Setup accessible (or protected) attributes for your model
  attr_accessor :login, :avatar
  attr_accessible :email, :password, :password_confirmation, :remember_me, :username, :login, :faculty_id, :firstname, 
                  :surname, :about, :search, :avatar
  has_attached_file :avatar, :styles => { :medium => "300x300#", :thumb => "200x200#" }, :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/
  validates_attachment_size :avatar, :less_than => 2.megabytes
  # attr_accessible :title, :body

  def forem_name
    email
  end

  #def forem_email
  #  email_address
  #end
  
  def self.find_for_database_authentication(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      where(conditions.to_hash).where(["lower(username) = :value OR lower(email) = :value", { :value => login.downcase }]).first
    else
      where(conditions.to_hash).first
    end
  end 

  def self.search(search)
    if search
      find(:all, :conditions => ['surname LIKE ?', "%#{search}%"])
    else
      find(:all)
    end
  end

end
