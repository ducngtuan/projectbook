class Faculty < ActiveRecord::Base
  belongs_to :user
  
  has_many :project

  attr_accessible :name
end
