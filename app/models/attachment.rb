class Attachment < ActiveRecord::Base
  belongs_to :user, foreign_key: "user_id"
  belongs_to :project, foreign_key: "project_id"

  attr_accessor :file
  attr_accessible :user_id, :file, :project_id
  
  has_attached_file :file
  validates_attachment_size :file, :less_than => 50.megabytes
  validates_attachment_presence :file

end
